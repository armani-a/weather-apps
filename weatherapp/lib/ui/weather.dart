import 'dart:async';
import 'dart:convert' show json;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../utilities/util.dart' as util;

class Weather extends StatefulWidget {
  @override
  _WeatherState createState() => _WeatherState();
}

class _WeatherState extends State<Weather> {
  String _cityInput;

  Future _goToNextScreen(BuildContext context) async {
    Map entry = await Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) {
      return new EnterCity();
    }));

    if (entry != null && entry.containsKey('enter')) {
      _cityInput = entry['enter'];
//      print(entry['enter'].toString());
    }
  }

  void info() async {
    Map data = await getWeather(util.appId, util.defaultCity);
    print(data.toString());
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Weather'),
          backgroundColor: Colors.red,
          centerTitle: true,
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.menu),
                onPressed: () {
                  _goToNextScreen(context);
                })
          ],
        ),

        //background image
        body: new Stack(children: <Widget>[
          new Center(
            child: new Image.asset(
              'images/weather_tree.jpg',
              width: 490.0,
              height: 1300.0,
              fit: BoxFit.fill,
            ),
          ),

          //container for text
          new Container(
            alignment: Alignment.topRight,
            margin: const EdgeInsets.fromLTRB(0.0, 10.9, 20.9, 0.0),
            child: new Text(
              '${_cityInput == null ? util.defaultCity : _cityInput}',
              style: city(),
            ),
          ),

          //container for icon
          new Container(
            alignment: Alignment.topCenter,
            child: new Image.asset('images/weather_icon.jpg'),
          ),
          updateWidget(_cityInput),
        ]));
  }

  //api
  Future<Map> getWeather(String appId, String city) async {
    String url = 'http://api.openweathermap.org/data/2.5/weather?q=$city&appid='
        '${util.appId}&units=metric';

    http.Response response = await http.get(url);

    return json.decode(response.body);
  }

//parsing json to get the data needed
  Widget updateWidget(String city) {
    return new FutureBuilder(
        future: getWeather(util.appId, city == null ? util.defaultCity : city),
        builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
          if (snapshot.hasData) {
            Map dataSet = snapshot.data;
            return new Container(
              margin: const EdgeInsets.fromLTRB(30, 260, 0, 0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new ListTile(
                    title: new Text(
                      dataSet['main']['temp'].toString() + "C",
                      style: new TextStyle(
                          fontSize: 50,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),
                    subtitle: new ListTile(
                      title: new Text(
                        "Humidity: ${dataSet['main']['humidity'].toString()}\n"
                            "Min_temp: ${dataSet['main']['temp_min'].toString()} C\n"
                            "Max_temp: ${dataSet['main']['temp_max'].toString()} C",
                        style: moreData(),
                      ),
                    ),
                  )
                ],
              ),
            );
          } else {
            return new Container();
          }
        });
  }
}

class EnterCity extends StatelessWidget {
  var _fieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue,
        title: new Text('Enter City'),
        centerTitle: true,
      ),
      body: new Stack(
        children: <Widget>[
          new Center(
            child: new Image.asset(
              'images/snow.jpg',
              width: 500,
              height: 1300,
              fit: BoxFit.fill,
            ),
          ),
          new ListView(
            children: <Widget>[
              new ListTile(
                title: new TextField(
                  decoration: new InputDecoration(
                    hintText: 'Enter City',
                  ),
                  controller: _fieldController,
                  keyboardType: TextInputType.text,
                ),
              ),
              new ListTile(
                title: new FlatButton(
                    onPressed: () {
                      Navigator.pop(context, {'enter': _fieldController.text});
                    },
                    color: Colors.blue,
                    textColor: Colors.white,
                    child: new Text('Get Weather')),
              )
            ],
          )
        ],
      ),
    );
  }
}

TextStyle city() {
  return new TextStyle(
      color: Colors.black, fontStyle: FontStyle.italic, fontSize: 23);
}

TextStyle tempStyle() {
  return new TextStyle(
      color: Colors.white,
      fontSize: 50,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500);
}

TextStyle moreData() {
  return new TextStyle(
      color: Colors.white, fontStyle: FontStyle.normal, fontSize: 25);
}

