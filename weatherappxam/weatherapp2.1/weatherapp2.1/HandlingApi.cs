﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;

namespace weatherapp2._1
{
    public class HandlingApi
{
    public async Task<Weather> GetMainData(string query)
        {
            HttpClient client = new HttpClient();
            Weather weatherData = null;
            try
            {
                var response = await client.GetAsync(query);
                
                if (response.IsSuccessStatusCode)
                {
                    string content = response.Content.ReadAsStringAsync().Result;
                    
                    Console.WriteLine("Request Message Information:- \n\n" + response.RequestMessage + "\n");
                    Console.WriteLine("Response Message Header \n\n" + response.Content.Headers + "\n");
                    // Get the response
                    var customerJsonString = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("Your response data is: " + customerJsonString);
                    Console.WriteLine("CONTENT /////////////////// " + content);
                    // Deserialise the data 
                    weatherData = JsonConvert.DeserializeObject<Weather>(content, new Main());
                    // Do something with it
                   


                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\t\tERROR {0}", ex.Message);
            }

            return weatherData;
        }
    }
}

