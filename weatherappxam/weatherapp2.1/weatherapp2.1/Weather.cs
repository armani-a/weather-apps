﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Newtonsoft.Json;

namespace weatherapp2._1 {
    public class Weather
    {
        [JsonProperty("temp")]
        public double Temperature { get; set; }

        [JsonProperty("humidity")]
        public long Humidity { get; set; }

        [JsonProperty("temp_min")]
        public double MinTemp { get; set; }

        [JsonProperty("temp_max")]
        public double MaxTemp { get; set; }


    }
}