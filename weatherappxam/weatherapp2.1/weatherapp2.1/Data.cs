﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace weatherapp2._1
{

    public class Main : JsonConverter
    {

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Weather));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            Newtonsoft.Json.Linq.JObject dataObject = Newtonsoft.Json.Linq.JObject.Load(reader);
            Weather data = dataObject.SelectToken("main").ToObject<Weather>();

            return data;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
