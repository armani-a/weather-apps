﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace weatherapp2._1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        HandlingApi _handlingApi;
        public MainPage()
        {
            InitializeComponent();
            _handlingApi = new HandlingApi();

        }

        async void OnGetWeatherButtonClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_cityEntry.Text))
            {
                Weather weatherData = await _handlingApi.GetMainData(GenerateRequestUri(Api.OpenWeatherLink));
                BindingContext = weatherData;
            }
        }

        string GenerateRequestUri(string endpoint)
        {
            string requestUri = endpoint;
            requestUri += $"?q={_cityEntry.Text}";
            requestUri += $"&APPID={Api.OpenWeatherAPIKey}";
            requestUri += "&units=metric";
            return requestUri;
        }
    }
}

    